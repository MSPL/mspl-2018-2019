# Mini-Projet

The Mini-Projet has to be developed in *groups of two students*.

## Datasets

List where data sets have to be chosen from:

- FR Data.gouv: https://www.data.gouv.fr/fr/
- Insee: https://www.insee.fr/fr/accueil
- Kaggle: https://www.kaggle.com/datasets
- US Data.gov: https://catalog.data.gov/dataset


## Guidelines

1. Justify the data set selection and described it
2. Formulate three questions about the selected data set
3. Discuss with the professors and choose one question among these
4. Propose a methodology to answer that question
5. Implement this methodology using Literate Programming
6. The report must be acessible online (in your git repository)

## Report

The R Markdown (to be written in RStudio) must contain:

- Frontpage
  - _Title_
  - Student's name
- Table of contents
- Introduction
  - Context / Dataset description
    - How the dataset has been obtained?
  - Description of the question
- Methodology
  - Data clean-up procedures
  - Scientific workflow
  - Data representation choices
- Analysis in Literate Programming
- Conclusion
- References

## Important notes

The question might be from simple to complex. More the question is
simple, more deep should be the analysis.

## Groups

Your project defence will be composed of (strictly) 5 minutes of presentation
and 2 minutes of questions. The presentation support could be anything: if you
have a PDF, you have to provide it to us in advance, if you need your own
machine be prepared (setup time is part of your defence time).

| id | Group | Time  | Teams                                       |Theme |   Repository | Rapport
|----|-------|-------|----------------------------------------------|---------------|-------------------------|-----
|1|2| 09:42|Wael Mazen/Tetrel Loan |   Vente de jeux video | https://gricad-gitlab.univ-grenoble-alpes.fr/waelm/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/waelm/mspl-2018-2019/blob/master/Activities/Projet%20WAEL-TETREL/L3-MIAGE-MSPL-WAEL-TETREL.pdf
|2|2| 10:00| Lemaire Loïc / Lestani Robinson | Apps Googlestore| https://gricad-gitlab.univ-grenoble-alpes.fr/lestanir/mspl-2018-2019|https://gricad-gitlab.univ-grenoble-alpes.fr/lestanir/mspl-2018-2019/blob/master/Projet/L3-MIAGE-MSPL-LESTANI-LEMAIRE.pdf
|3|2| 10:07| Vailhère Théo /Meliard Quentin | Consommation d'alcool  | https://gricad-gitlab.univ-grenoble-alpes.fr/meliardq/mspl-2018-2019
|4|2| 10:14| Ghammaz Ayoub / Orand Régis | Séismes | https://gricad-gitlab.univ-grenoble-alpes.fr/orandr/mspl-2018-2019 | CF mail ...
|5|2| 10:21| Madide Adam / Phan Dhanh |Indice du boheur | https://gricad-gitlab.univ-grenoble-alpes.fr/madidea/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/madidea/mspl-2018-2019/blob/master/Projet/L3-MIAGE-MSPL-MADIDE-PHAN.pdf
|6|2| 10:28| Fall Ayy / Sagara Idriss  | Performance étudiants| https://gricad-gitlab.univ-grenoble-alpes.fr/sagarai/MSPL | https://gricad-gitlab.univ-grenoble-alpes.fr/sagarai/MSPL/blob/master/Projet/ProjetR_Student_performance.Rmd
|7|2| 10:35| Mathieu Yacine / Martins Benoit | Taux de chomage | https://gricad-gitlab.univ-grenoble-alpes.fr/mathieya/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/mathieya/mspl-2018-2019/blob/master/Projet/Projet_Stats_MARTINS_MATHIEU.pdf
|8|2| 10:42| Diagne Khadidiatou / Thiam Cheikh | Fifa-sport |https://gricad-gitlab.univ-grenoble-alpes.fr/diagnek/mspl-2018-2019 | CF mail
|9|2| 10:49| Sajides Soufiane / Bernes Jules | Evolution des crimes en France| https://gricad-gitlab.univ-grenoble-alpes.fr/bernesj/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/bernesj/mspl-2018-2019/blob/master/Activities/Project/script.pdf
|10|1| 08:00| Jlassis Imene / Kadidiatou Coulibaly | Taux de Chômage | https://gricad-gitlab.univ-grenobles-alpes.fr/coulikb/mspl-2018-2019 |
|11|1| 08:07| Romain Baills / Firsov Oleksandr | Google play store |https://gricad-gitlab.univ-grenoble-alpes.fr/baillsr/mspl-2018-2019 | 
|12|1| 08:14| Khadir Mehdi / Zouir Yamini | Accident corporel de la circulation | https://gricad-gitlab.univ-grenobles-alpes.fr/khadirm/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/khadirm/mspl-2018-2019/blob/master/projet/projet_stat_Mehdi_Zoher.pdf
|13|1| 08:21| Jolan Daumas / Blaye Thibault | Kickstatrter | https://gricad-gitlab.univ-grenobles-alpes.fr/daumasj/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/daumasj/mspl-2018-2019/blob/master/Projet/L3-MIAGE-MSPL-DAUMAS-BLAYE.html
|14|2| 10:56| Jovanovic Loic / Mourthadhoi Sultan | Les jeux olympiques | https://gricad-gitlab.univ-grenobles-alpes.fr/mourthas/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/mourthas/mspl-2018-2019/blob/master/Projet/L3-MIAGE-MSPL-MOURTHADHOI-JOVANOVIC.pdf
|15|?| 08:28| Lucille Guillaume / Cedric Hannequin | Etude films | https://gricad-gitlab.univ-grenoble-alpes.fr/guillalu/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/guillalu/mspl-2018-2019/blob/master/Projet/L3-MIAGE-MSPL-Hannequin-Guillaume.pdf.
|16|?| 08:35| Hanh Do / ??? | Changements climatiques | https://gricad-gitlab.univ-grenoble-alpes.fr/dothit/mspl-2018-2019 |https://gricad-gitlab.univ-grenoble-alpes.fr/dothit/mspl-2018-2019/blob/master/Projet/L3-MIAGE-MSPL-DO-CISSE/L3-MIAGE-MSPL-DO-CISSE.pdf
|17|?| 08:42| Emre AYDIN / elhadj amadou korka bah | ?? | https://gricad-gitlab.univ-grenoble-alpes.fr/aydine/mspl-2018-2019/ | https://gricad-gitlab.univ-grenoble-alpes.fr/aydine/mspl-2018-2019/blob/8a8dbdebabd466f48e059db6f7f1c2f580124066/Projet/L3-MIAGE-MSPL-Aydin-Elhadj.pdf
|18|?| 09:00| Mathis Ruffieux | Rejet de CO2 & dérèglement climatique | https://gricad-gitlab.univ-grenoble-alpes.fr/ruffieum/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/ruffieum/mspl-2018-2019/blob/master/L3-MIAGE-MSPL-RUFFIEUX.Rmd
|19|?| 09:07| Massinissa Bitous / BOUAZIZ Lamine | Taux de suicide en France | ?? | https://gricad-gitlab.univ-grenoble-alpes.fr/bitousm/projetstatfinal/blob/master/L3-MIAGE-MSPL-Bouaziz_Bitous.pdf
|20|?| 09:14|  Belahadji Ilyes / ElBahraoui Imade | Niveaux scolaire | https://gricad-gitlab.univ-grenoble-alpes.fr/belahadi/ | https://gricad-gitlab.univ-grenoble-alpes.fr/belahadi/projet-stat/blob/master/L3-MIAGE-MSPL-Belahadji-ElBahraoui._Rapport_Stats.Rmd
|21|?| 09:21| PERRIN Alexandre / MENDES Julien | Jeux vidéos | https://gricad-gitlab.univ-grenoble-alpes.fr/perrinal/mspl-2018-2019/  | https://gricad-gitlab.univ-grenoble-alpes.fr/perrinal/mspl-2018-2019/blob/master/PROJET/L3-MIAGE-MSPL-PERRIN-MENDES.pdf
|22|?| 09:28| Tariq HASDI | Films | ??? | CF mail
|23|?| 09:35| Abderrahmane Bekkouch / Soufiane Kabad | Nombre Enteprise |   https://gricad-gitlab.univ-grenoble-alpes.fr/bekkouca/mspl-2018-2019 | https://gricad-gitlab.univ-grenoble-alpes.fr/bekkouca/mspl-2018-2019/blob/master/Projet%20MSPL/Projet2019-AB-SK.pdf
|24|?| 10:56| Lamine Imad Eddine / Housbane Soraya | Éducation dans le monde |  https://gricad-gitlab.univ-grenoble-alpes.fr/laminei/mspl-2018-2019 |  https://gricad-gitlab.univ-grenoble-alpes.fr/laminei/mspl-2018-2019/blob/master/LAMINE_HOUSBANE/Rapport.pdf
